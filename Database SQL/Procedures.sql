--------------------------------------------------------
--  File created - Monday-December-31-2018   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Procedure EXM_ADD_ACCOUNT
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "COOP_SDU"."EXM_ADD_ACCOUNT" 
(
 
ACC_ID IN VARCHAR2,
ACC_PERSON_NAME IN VARCHAR2,
ACC_BALANCE IN FLOAT,
ACC_BRANCH IN VARCHAR2,
ACC_OWNER_NAME IN VARCHAR2,

ACC_DETAIL_CURSOR OUT SYS_REFCURSOR

)
AS BEGIN
 
 insert into COOP_SDU.ACCOUNTS (ID,PERSONNAME,BALANCE,BRANCH,OWNERNAME) values (ACC_ID,ACC_PERSON_NAME,ACC_BALANCE,ACC_BRANCH,ACC_OWNER_NAME) ;
 OPEN ACC_DETAIL_CURSOR for
 SELECT ID,PERSONNAME,BALANCE,BRANCH,OWNERNAME  FROM COOP_SDU.ACCOUNTS where ID=ACC_ID;
END EXM_ADD_ACCOUNT;

/
--------------------------------------------------------
--  DDL for Procedure EXM_DELETE_ACCOUNT
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "COOP_SDU"."EXM_DELETE_ACCOUNT" 
(
ACC_ID IN VARCHAR2 ,
ACC_DETAIL_CURSOR OUT SYS_REFCURSOR
)
AS 
BEGIN
  delete from COOP_SDU.ACCOUNTS where ID=ACC_ID;
  OPEN ACC_DETAIL_CURSOR for
  SELECT ID ,
PERSONNAME ,
BALANCE ,
BRANCH ,
OWNERNAME   FROM COOP_SDU.ACCOUNTS where ID=ACC_ID;
END EXM_DELETE_ACCOUNT;

/
--------------------------------------------------------
--  DDL for Procedure EXM_GETACCOUNTDETAILS
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "COOP_SDU"."EXM_GETACCOUNTDETAILS" 

(
ACC_ID IN VARCHAR2,
ACC_DETAIL_CURSOR OUT SYS_REFCURSOR
)
AS 
BEGIN

  OPEN ACC_DETAIL_CURSOR for
  SELECT ID,PERSONNAME,BALANCE,BRANCH,OWNERNAME FROM COOP_SDU.ACCOUNTS where ID=ACC_ID;
  
END EXM_GETACCOUNTDETAILS;

/
--------------------------------------------------------
--  DDL for Procedure EXM_GETACCOUNTS
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "COOP_SDU"."EXM_GETACCOUNTS" 
(
   ACC_CURSOR OUT SYS_REFCURSOR    
)
AS 
BEGIN
open ACC_CURSOR for
  SELECT ID,PERSONNAME,BALANCE,BRANCH,OWNERNAME  FROM COOP_SDU.ACCOUNTS ORDER BY ID;
END EXM_GETACCOUNTS;

/
--------------------------------------------------------
--  DDL for Procedure EXM_UPDATE_ACCOUNT
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "COOP_SDU"."EXM_UPDATE_ACCOUNT" 
(
 
ACC_ID IN VARCHAR2,
ACC_PERSON_NAME IN VARCHAR2,
ACC_BALANCE IN FLOAT,
ACC_BRANCH IN VARCHAR2,
ACC_OWNER_NAME IN VARCHAR2,

ACC_DETAIL_CURSOR OUT SYS_REFCURSOR

)
AS BEGIN
 
 UPDATE COOP_SDU.ACCOUNTS SET PERSONNAME=ACC_PERSON_NAME,BALANCE=ACC_BALANCE,BRANCH=ACC_BRANCH,OWNERNAME=ACC_OWNER_NAME where ID=ACC_ID;
 OPEN ACC_DETAIL_CURSOR for
  SELECT ID,PERSONNAME,BALANCE,BRANCH,OWNERNAME  FROM COOP_SDU.ACCOUNTS where ID=ACC_ID;
  
END EXM_UPDATE_ACCOUNT;

/
--------------------------------------------------------
--  DDL for Procedure PSP_LOGIN
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "COOP_SDU"."PSP_LOGIN" 
(
USERNAME_GET IN VARCHAR2,
PASSWORD_GET IN VARCHAR2,
USER_DETAIL_CURSOR OUT SYS_REFCURSOR
)
AS 
BEGIN
 OPEN USER_DETAIL_CURSOR for
 SELECT USERNAME,ROLE,ID,FULLNAME FROM COOP_SDU.USER_LOGIN WHERE USERNAME=USERNAME_GET AND PASSWORD=PASSWORD_GET;
END PSP_LOGIN;

/
