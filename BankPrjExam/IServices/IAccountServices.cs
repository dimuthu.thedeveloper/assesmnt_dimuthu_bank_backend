﻿using BankPrjExam.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BankPrjExam.IServices
{
    public interface IAccountServices
    {
        Task<Account> AddAccount(Account acc);

        Task<string> DeleteAccount(string getid);

        Task<Account> GetAccountByID(string getid);

        Task<List<Account>> GetAccounts();

        Task<Account> UpdateAccount(Account acc);



    }
}
