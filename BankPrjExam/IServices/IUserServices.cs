﻿using BankPrjExam.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BankPrjExam.IServices
{
    public interface IUserServices
    {
        Task<User> Authanticate(string Username, string Password);

    }
}
