﻿using Microsoft.Extensions.Configuration;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace BankPrjExam.OracleDB
{
    public class DBContext
    {
        IConfiguration configuration;

        public DBContext(IConfiguration _config)
        {

            configuration = _config;

        }


        public IDbConnection GetConnection()
        {
            
            var ConnectionString = configuration.GetSection("ConnectionStrings").GetSection("ProductConnection").Value;
            var conn = new OracleConnection(ConnectionString);
            return conn;

        }
    }
}
