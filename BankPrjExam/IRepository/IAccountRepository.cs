﻿using BankPrjExam.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BankPrjExam.IRepository
{
    public interface IAccountRepository
    {
        Task<List<Account>> GetAccounts();

        Task<Account> GetAccountByID(string getid);

        Task<Account> AddAccount(Account acc);

        Task<string> DeleteAccountt(string getid);

        Task<Account> UpdateAccount(Account acc);
    }
}
