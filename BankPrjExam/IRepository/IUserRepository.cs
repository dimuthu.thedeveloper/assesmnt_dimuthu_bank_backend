﻿using BankPrjExam.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BankPrjExam.IRepository
{
    public interface IUserRepository
    {
        Task<User> LoginUser(string user, string passw);
    }
}
