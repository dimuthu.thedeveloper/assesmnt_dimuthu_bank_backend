﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BankPrjExam.Models
{
    public class User
    {
        public string id { get; set; }

        public string fullname { get; set; }

        public string username { get; set; }

        public string password { get; set; }

        public string token { get; set; }

        public string role { get; set; }
    }
}
