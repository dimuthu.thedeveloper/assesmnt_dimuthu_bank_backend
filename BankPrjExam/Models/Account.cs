﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BankPrjExam.Models
{
    public class Account
    {
        public string id { get; set; }

        public string personname { get; set; }

        public double balance { get; set; }

        public string branch { get; set; }

        public string ownername { get; set; }


    }
}
