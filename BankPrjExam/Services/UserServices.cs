﻿using BankPrjExam.IRepository;
using BankPrjExam.IServices;
using BankPrjExam.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BankPrjExam.Services
{
    public class UserServices: IUserServices
    {
        IConfiguration configuration;
        IUserRepository userRepo;

        public UserServices(IConfiguration _config, IUserRepository urepo)
        {

            configuration = _config;
            userRepo = urepo;
        }

        private List<User> Users = new List<User>()
        {
        };


        public async Task<User> Authanticate(string Username, string Password)
        {

            User user = await userRepo.LoginUser(Username, Password);
            if (user == null)
            {
                return null;
            }

             
            return user;


        }


    }
}
