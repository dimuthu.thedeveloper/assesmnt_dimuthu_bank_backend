﻿using BankPrjExam.IRepository;
using BankPrjExam.IServices;
using BankPrjExam.Models;
using BankPrjExam.Repository;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BankPrjExam.Services
{
    public class AccountServices: IAccountServices
    {

        IConfiguration configuration;
        IAccountRepository AccRepository;

        public AccountServices(IConfiguration _config, IAccountRepository accRepo)
        {

            configuration = _config;
            AccRepository = accRepo;
        }
                                  

        public async Task<Account> AddAccount(Account acc)
        {
            

            var addedAcc = await AccRepository.AddAccount(acc);

            return addedAcc;


        }

        public async Task<string> DeleteAccount(string getid)
        {
 


            var deleteAcc = await AccRepository.DeleteAccountt(getid);

            return deleteAcc;




        }

        public async Task<Account> GetAccountByID(string getid)
        {

             
            var getAccount = await AccRepository.GetAccountByID(getid);

            return getAccount;


        }

        public async Task<List<Account>> GetAccounts()
        {

             

            var GetAccounts = await AccRepository.GetAccounts();

            return GetAccounts;

        }

        public async Task<Account> UpdateAccount(Account acc)
        {
             


            var GetUpdated = await AccRepository.UpdateAccount(acc);

            return GetUpdated;


        }

        

    }
}

