﻿using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace BankPrjExam.AuthorizationHandler
{
    public class RoleRequirementHandler : AuthorizationHandler<RoleRequirement>
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, RoleRequirement requirement)
        {

            if (!context.User.HasClaim(c => c.Type == ClaimTypes.Role))
            {
                return Task.CompletedTask;
            }
            else
            {

                var role = context.User.FindFirst(c => c.Type == ClaimTypes.Role).Value;

                if (role.Equals(requirement.Role, StringComparison.OrdinalIgnoreCase))
                {
                    context.Succeed(requirement);
                }
                return Task.CompletedTask;

            }

        }
    }
}
