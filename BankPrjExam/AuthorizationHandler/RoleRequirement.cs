﻿using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BankPrjExam.AuthorizationHandler
{
    public class RoleRequirement : IAuthorizationRequirement
    {

        public RoleRequirement(string role)
        {

            this.Role = role;

        }

        public string Role { get; }


    }
}
