﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BankPrjExam.IServices;
using BankPrjExam.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BankPrjExam.Controller
{
    [Authorize]
    [Route("v1.0/accounts")]
    [ApiController]
    public class AccountController : ControllerBase
    {

        IAccountServices AccServices;

        public AccountController(IAccountServices AccServices)
        {

            this.AccServices = AccServices;

        }



        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> GetAllAccounts()
        {
            var GetAcc = await AccServices.GetAccounts();


            return Ok(GetAcc);
        }


        [Route("{id}")]
        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> GetProductById(String id)
        {
            //*
            var product = await AccServices.GetAccountByID(id);



            if (product != null)
            {
                return Ok(product);
            }
            else
            {
                return NotFound("No Account !");
            }

        }



        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Addproduct([FromBody]Account accnt)
        {
            var AddedAccount = (await AccServices.AddAccount(accnt));

            if (AddedAccount == null)
            {
                return NotFound("Could not add ! \nAccount is not found with given id !");
            }
            else
            {
                return Ok(AddedAccount);
            }

        }




        ////
        [HttpPut]
        [Authorize(Policy = "AdminOnly")]
        public async Task<IActionResult> UpdateProduct([FromBody]Account acc)
        {
            var prodyctUpdt = await AccServices.UpdateAccount(acc);

            if (prodyctUpdt == null)
            {
                return NotFound("Account is not found with given id !");
            }
            else
            {
                return Ok("Successfully updated !");
            }
        }


     

        [HttpDelete("{id}")]
        [Authorize(Policy = "AdminOnly")]
        public async Task<IActionResult> DeleteAccount(String id)
        {
            string AccountDeleted = await AccServices.DeleteAccount(id);

            if (AccountDeleted.Equals("ok"))
            {
                return Ok("Deleted !");
            }
            else if (AccountDeleted.Equals("not found"))
            {
                return NotFound("Account is not found with given id !");
            }
            else if (AccountDeleted.Equals("not deleted"))
            {

                return NotFound("Could not delete !");
            }
            else
            {

                return StatusCode(500);
            }
        }

    }
}