﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BankPrjExam.IServices;
using BankPrjExam.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BankPrjExam.Controller
{
    [Authorize]
    [Route("v1.0/users")]
    [ApiController]
    public class UserController : ControllerBase
    {

        IUserServices _userService;

        public UserController(IUserServices userServices)
        {

            _userService = userServices;
        }

        

        [AllowAnonymous]
        [HttpPost("authenticate")]
        public async Task<IActionResult> Authenticate([FromBody]UserCredential userCredential)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var user = await _userService.Authanticate(userCredential.UserName, Encryptor.MD5Hash(userCredential.PassWord));
            if (user == null)
            {
                return BadRequest("Username or password is incorrect");
            }
            return Ok(user);
        }

    }
}