﻿using BankPrjExam.IRepository;
using BankPrjExam.Models;
using BankPrjExam.OracleDB;
using Dapper;
using Microsoft.Extensions.Configuration;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace BankPrjExam.Repository
{
    public class AccountRepository : IAccountRepository
    {

        IConfiguration configuration;

        public AccountRepository(IConfiguration _config)
        {

            configuration = _config;
        }

        public async Task<Account> AddAccount(Account acc)
        {
            Account result = null;


            try
            {
                var dyParam = new OracleDynamicParameters();

                dyParam.Add("ACC_ID", OracleDbType.Varchar2, ParameterDirection.Input, Guid.NewGuid().ToString());
                dyParam.Add("ACC_PERSON_NAME", OracleDbType.Varchar2, ParameterDirection.Input, acc.personname);
                dyParam.Add("ACC_BALANCE", OracleDbType.Double, ParameterDirection.Input, acc.balance);
                dyParam.Add("ACC_BRANCH", OracleDbType.Varchar2, ParameterDirection.Input, acc.branch);
                dyParam.Add("ACC_OWNER_NAME", OracleDbType.Varchar2, ParameterDirection.Input, acc.ownername);
                dyParam.Add("ACC_DETAIL_CURSOR", OracleDbType.RefCursor, ParameterDirection.Output);

                var DBCon = new DBContext(configuration);
                var conn = DBCon.GetConnection();
                if (conn.State == ConnectionState.Closed)
                {

                    conn.Open();

                }

                if (conn.State == ConnectionState.Open)
                {

                    var query = "EXM_ADD_ACCOUNT";


                    result = (await SqlMapper.QueryAsync<Account>(conn, query, param: dyParam, commandType: CommandType.StoredProcedure))?.FirstOrDefault();

                }



            }
            catch (Exception ex)
            {
                throw ex;
            }

            if (result == null)
            {
                return null;
            }


            return result;
        }

        public async Task<string> DeleteAccountt(string getid)
        {
            try
            {
                var dyParam = new OracleDynamicParameters();
                dyParam.Add("ACC_ID", OracleDbType.Varchar2, ParameterDirection.Input, getid);
                dyParam.Add("ACC_DETAIL_CURSOR", OracleDbType.RefCursor, ParameterDirection.Output);

                var dyParam2 = new OracleDynamicParameters();        // for validation 
                dyParam2.Add("ACC_ID", OracleDbType.Varchar2, ParameterDirection.Input, getid);
                dyParam2.Add("ACC_DETAIL_CURSOR", OracleDbType.RefCursor, ParameterDirection.Output);


                var DBCon = new DBContext(configuration);
                var conn = DBCon.GetConnection();
                if (conn.State == ConnectionState.Closed)
                {

                    conn.Open();

                }

                if (conn.State == ConnectionState.Open)
                {

                    var query = "EXM_DELETE_ACCOUNT";
                    var query2 = "EXM_GETACCOUNTDETAILS";

                    Account result = null;

                    result = (await SqlMapper.QueryAsync<Account>(conn, query2, param: dyParam2, commandType: CommandType.StoredProcedure))?.FirstOrDefault();

                    if (result != null)
                    {

                        Account result2 = null;

                        result2 = (await SqlMapper.QueryAsync<Account>(conn, query, param: dyParam, commandType: CommandType.StoredProcedure))?.FirstOrDefault();

                        if (result2 == null)
                        {
                            return "ok";
                        }
                        else
                        {
                            return "not deleted";
                        }
                    }
                    else
                    {

                        return "not found";

                    }



                }
                else
                {
                    return "500";
                }



            }
            catch (Exception ex)
            {

                throw ex;

            }
        }

        public async Task<Account> GetAccountByID(string getid)
        {
            Account result = null;
            try
            {
                var dyParam = new OracleDynamicParameters();
                dyParam.Add("ACC_ID", OracleDbType.Varchar2, ParameterDirection.Input, getid);
                dyParam.Add("ACC_DETAIL_CURSOR", OracleDbType.RefCursor, ParameterDirection.Output);

                var DBCon = new DBContext(configuration);
                var conn = DBCon.GetConnection();
                if (conn.State == ConnectionState.Closed)
                {

                    conn.Open();

                }

                if (conn.State == ConnectionState.Open)
                {

                    var query = "EXM_GETACCOUNTDETAILS";

                    result = (await SqlMapper.QueryAsync<Account>(conn, query, param: dyParam, commandType: CommandType.StoredProcedure))?.FirstOrDefault();


                }



            }
            catch (Exception ex)
            {
                throw ex;
            }

            if (result == null)
            {
                return null;
            }



            return result;

        }

        public async Task<List<Account>> GetAccounts()
        {
            List<Account> result = null;
            try
            {
                var dyParam = new OracleDynamicParameters();
                dyParam.Add("ACC_CURSOR", OracleDbType.RefCursor, ParameterDirection.Output);

                var DBCon = new DBContext(configuration);
                var conn = DBCon.GetConnection();
                if (conn.State == ConnectionState.Closed)
                {

                    conn.Open();

                }

                if (conn.State == ConnectionState.Open)
                {

                    var query = "EXM_GETACCOUNTS";

                    result = (await SqlMapper.QueryAsync<Account>(conn, query, param: dyParam, commandType: CommandType.StoredProcedure)).ToList();

                }



            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        public async Task<Account> UpdateAccount(Account acc)
        {
            Account result = null;
            try
            {
                var dyParam = new OracleDynamicParameters();

                dyParam.Add("ACC_ID", OracleDbType.Varchar2, ParameterDirection.Input, acc.id);
                dyParam.Add("ACC_PERSON_NAME", OracleDbType.Varchar2, ParameterDirection.Input, acc.personname);
                dyParam.Add("ACC_BALANCE", OracleDbType.Double, ParameterDirection.Input, acc.balance);
                dyParam.Add("ACC_BRANCH", OracleDbType.Varchar2, ParameterDirection.Input, acc.branch);
                dyParam.Add("ACC_OWNER_NAME", OracleDbType.Varchar2, ParameterDirection.Input, acc.ownername);

                dyParam.Add("ACC_DETAIL_CURSOR", OracleDbType.RefCursor, ParameterDirection.Output);

                var DBCon = new DBContext(configuration);
                var conn = DBCon.GetConnection();
                if (conn.State == ConnectionState.Closed)
                {

                    conn.Open();

                }

                if (conn.State == ConnectionState.Open)
                {

                    var query = "EXM_UPDATE_ACCOUNT";

                    result = (await SqlMapper.QueryAsync<Account>(conn, query, param: dyParam, commandType: CommandType.StoredProcedure))?.FirstOrDefault();


                }



            }
            catch (Exception ex)
            {
                throw ex;
            }

            if (result == null)
            {
                return null;
            }

            return result;
        }
    }
}
