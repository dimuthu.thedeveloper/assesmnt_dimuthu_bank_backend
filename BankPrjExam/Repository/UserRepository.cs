﻿using BankPrjExam.IRepository;
using BankPrjExam.Models;
using BankPrjExam.OracleDB;
using Dapper;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace BankPrjExam.Repository
{
    public class UserRepository : IUserRepository
    {
        IConfiguration configuration;



        public UserRepository(IConfiguration _config)
        {

            configuration = _config;

        }



        public async Task<User> LoginUser(string user, string passw)
        {

            User result = null;
            try
            {
                var DBCon = new DBContext(configuration);

                var dyParam = new OracleDynamicParameters();
                dyParam.Add("USERNAME_GET", OracleDbType.Varchar2, ParameterDirection.Input, user);
                dyParam.Add("PASSWORD_GET", OracleDbType.Varchar2, ParameterDirection.Input, passw);
                dyParam.Add("USER_DETAIL_CURSOR", OracleDbType.RefCursor, ParameterDirection.Output);
                var conn = DBCon.GetConnection();
                if (conn.State == ConnectionState.Closed)
                {

                    conn.Open();

                }

                if (conn.State == ConnectionState.Open)
                {

                    var query = "PSP_LOGIN";

                    result = (await SqlMapper.QueryAsync<User>(conn, query, param: dyParam, commandType: CommandType.StoredProcedure))?.FirstOrDefault();


                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

            if (result == null)
            {
                return null;
            }

            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes("THIS IS USED TO SIGN AND VERIFY JWT TOKENS, REPLACE IT WITH YOUR OWN SECRET, IT CAN BE ANY STRING");
            var tokenDescripter = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[] {
                    new Claim(ClaimTypes.Name,result.id.ToString()),
                    new Claim(ClaimTypes.Role, result.role)
                }),

                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)

            };

            var token = tokenHandler.CreateToken(tokenDescripter);
            result.token = tokenHandler.WriteToken(token);
            result.password = null;

            return result;

        }
    }
}
